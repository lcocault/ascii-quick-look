/* Copyright 2021 Laurent COCAULT
 * Licensed to Laurent COCAULT under one or more contributor license agreements.
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. Laurent COCAULT licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.tools.aql;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;

/**
 * Ascii quick look main class.
 */
public class AsciiQuickLook {

    /** Size of the blocks to be replaced a character (width in pixels). */
    private static final int COLUMN_WIDTH = 8;
    /** Size of the blocks to be replaced a character (height in pixels). */
    private static final int ROW_HEIGHT = 16;

    /**
     * Display the image as a series of ASCII characters.
     * @param imageFile
     *            File containing the image
     * @throws IOException
     *             Error while reading the image
     */
    private static void displayImage(File imageFile) throws IOException {
        // Process the input
        FileInputStream fis = new FileInputStream(imageFile);
        try {
            // Get an image from the file
            Image image = new Image(fis);

            // Translate the image dimensions in ASCII rows and columns
            int nbColumns = (int) (image.getWidth() / COLUMN_WIDTH);
            int nbRows = (int) (image.getHeight() / ROW_HEIGHT);

            // Read the data block by block
            PixelReader reader = image.getPixelReader();
            for (int row = 0; row < nbRows; row++) {
                for (int col = 0; col < nbColumns; col++) {
                    System.out.print(map(reader, row * ROW_HEIGHT,
                            col * COLUMN_WIDTH, (row + 1) * ROW_HEIGHT,
                            (col + 1) * COLUMN_WIDTH));
                }
                System.out.println("");
            }

        } finally {
            fis.close();
        }
    }

    /**
     * Ascii Quick Look entry point.
     * @param args
     *            Arguments of the program
     *            <ol>
     *            <li>Image - Path of the image to display</li>
     *            </ol>
     */
    public static void main(String[] args) {

        // Check the argument count
        if (args.length != 1) {
            System.err.println(
                    "Expected one argument - path of the file to display");
            System.exit(-1);
        }

        // Check the argument validity
        File imageFile = new File(args[0]);
        if (!imageFile.canRead()) {
            System.err.println("Input file (" + args[0] + ") cannot be read");
            System.exit(-1);
        }

        // Display the image
        try {
            displayImage(imageFile);
        } catch (IOException e) {
            System.err.println(
                    "Cannot process the input file (" + e.getMessage() + ")");
        }
    }

    /**
     * Map a block from the image into a representative character.
     * @param reader
     *            Pixels block reader
     * @param rowStart
     *            Row index of the upper left pixel
     * @param columnStart
     *            Column index of the upper left pixel
     * @param rowEnd
     *            Row index of the lower right pixel
     * @param columnEnd
     *            Column index of the lower right pixel
     * @return A character that represents the block
     */
    private static char map(PixelReader reader, int rowStart, int columnStart,
            int rowEnd, int columnEnd) {

        // Compute the mean brightness of the block
        double brightness = 0.0;
        for (int row = rowStart; row < rowEnd; row++) {
            for (int col = columnStart; col < columnEnd; col++) {
                brightness += reader.getColor(col, row).getBrightness();
            }
        }
        double meanBrightness = brightness /
                ((rowEnd - rowStart) * (columnEnd - columnStart));

        // Map the brightness to a character
        char result = ' ';
        if (meanBrightness < 0.9) {
            result = '.';
        }
        if (meanBrightness < 0.5) {
            result = 'X';
        }

        return result;
    }

}
